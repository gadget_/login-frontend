import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Movie } from '../../interfaces/movie.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MoviesService {
  constructor(private http: HttpClient) {}

  getMovies(): Observable<any[]> {
    return this.http.get<any[]>(
      'https://moviesapp-and.herokuapp.com/api/movies'
    );
  }

  postMovies(data) {
    console.log(data);
    return this.http.post('http://localhost:5000/api/movies', data);
  }

  getMovieById(id) {
    return this.http.get<any>(
      `https://moviesapp-and.herokuapp.com/api/movies/${id}`
    );
  }

  calificar(id, calificacion) {
    return this.http.put<any>(
      `https://moviesapp-and.herokuapp.com/api/movies/calificacion/${id}`,
      calificacion
    );
  }

  comentar(id, comentario) {
    console.log(comentario);
    return this.http.put<any>(
      `https://moviesapp-and.herokuapp.com/api/movies/${id}`,
      comentario
    );
  }
}
