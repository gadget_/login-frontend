import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MoviesService } from '../../../services/movies/movies.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styles: [],
})
export class AddComponent implements OnInit {
  miformulario: FormGroup = this.fb.group({
    nombre: ['1'],
    director: ['1'],
    sinopsis: ['1'],
    file: [''],
    actores: this.fb.array([['brad pitt']]),
    canciones: this.fb.array([['macarena']]),
    trailerUrl: [''],
  });

  nuevoActor: FormControl = this.fb.control('');
  nuevaCancion: FormControl = this.fb.control('');
  constructor(private fb: FormBuilder, private MovieService: MoviesService) {}

  ngOnInit(): void {}

  get actoresArr() {
    return this.miformulario.get('actores') as FormArray;
  }

  get cancionesArr() {
    return this.miformulario.get('canciones') as FormArray;
  }

  agregarActor() {
    if (this.nuevoActor.invalid) {
      return;
    }

    this.actoresArr.push(new FormControl(this.nuevoActor.value));
    this.nuevoActor.reset();
  }
  borrarActor(i) {
    this.actoresArr.removeAt(i);
  }

  agregarCancion() {
    if (this.nuevaCancion.invalid) {
      return;
    }

    this.cancionesArr.push(new FormControl(this.nuevaCancion.value));
    this.nuevaCancion.reset();
  }
  borrarCancion(i) {
    this.cancionesArr.removeAt(i);
  }

  guardar() {
    this.MovieService.postMovies(this.miformulario.value).subscribe((resp) =>
      console.log(resp)
    );
  }
}
