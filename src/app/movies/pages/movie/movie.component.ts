import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from 'src/app/services/movies/movies.service';
import { FormBuilder, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styles: [],
})
export class MovieComponent implements OnInit {
  miformulario: FormGroup = this.fb.group({
    calificacion: [2],
  });

  formComment: FormGroup = this.fb.group({
    comentarios: [],
  });
  constructor(
    private activatedRoute: ActivatedRoute,
    private movieService: MoviesService,
    private fb: FormBuilder
  ) {}
  id;
  values;
  calificacion;
  comentarios;
  movie: any;
  ngOnInit(): void {
    this.activatedRoute.params.subscribe((resp) => (this.id = resp.id));
    this.movieService.getMovieById(this.id).subscribe((resp: any) => {
      this.movie = resp.result;
      ///////////////////////////////////77
      this.values = resp.result.calificacion;
      this.comentarios = resp.result.comentarios;
      let sum = this.values.reduce(
        (previous, current) => (current += previous)
      );
      let avg = Math.floor(sum / this.values.length);
      this.calificacion = avg;
      console.log(this.calificacion);
    });
  }

  calificar() {
    this.movieService
      .calificar(this.id, this.miformulario.value)
      .subscribe((resp) => {
        location.reload();
      });
  }
  comentar() {
    console.log(this.formComment.value);
    this.movieService
      .comentar(this.id, this.formComment.value)
      .subscribe((resp) => {
        console.log(resp);
        console.log(this.formComment.value);
        location.reload();
      });
  }
}
