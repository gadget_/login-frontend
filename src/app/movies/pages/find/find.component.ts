import { Component, OnInit } from '@angular/core';
import { Movie } from '../../../interfaces/movie.interface';
import { MoviesService } from 'src/app/services/movies/movies.service';
import { Observable, observable } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styles: [],
})
export class FindComponent implements OnInit {
  termino;
  movies: Movie[];
  movieSeleccionada;

  constructor(private movieService: MoviesService, private router: Router) {}

  ngOnInit(): void {}

  buscando() {
    this.movieService.getMovies().subscribe((resp: any) => {
      this.movies = resp.result;
    });
  }

  opcionSeleccionada(event: MatAutocompleteSelectedEvent) {
    const movie = event.option.value;
    this.router.navigateByUrl(`/movies/${movie._id}`);
  }
}
