import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/interfaces/movie.interface';
import { MoviesService } from 'src/app/services/movies/movies.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styles: [
    `
      mat-card {
        margin-top: 20px;
      }
    `,
  ],
})
export class ListComponent implements OnInit {
  constructor(private movieService: MoviesService) {}
  movies: Movie[];
  calificacion = [];
  ngOnInit(): void {
    this.movieService.getMovies().subscribe((resp: any) => {
      this.movies = resp.result;
      console.log(this.movies);
      //promedio de calificaciones
      resp.result.forEach((element) => {
        let values = element.calificacion;
        let sum = values.reduce((previous, current) => (current += previous));
        let avg = Math.floor(sum / values.length);
        this.calificacion.push(avg);
        console.log(this.calificacion);
      });
    });
  }
}
