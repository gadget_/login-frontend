export interface Movie {
  actores?: string[];
  canciones?: string[];
  comentarios?: string[];
  calificacion?: number[];
  _id?: string;
  sinopsis?: string;
  nombre?: string;
  posterUrl?: string;
  trailerUrl?: string;
  __v?: number;
}
